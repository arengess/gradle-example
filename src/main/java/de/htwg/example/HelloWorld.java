package de.htwg.example;

/**
 * Created by armin on 08/04/15.
 */
public class HelloWorld {
    public static void main(String[] args) {
        HelloWorld helloWorld = new HelloWorld();
        helloWorld.sayHello("Hello, world");
    }

    public void sayHello(String text) {
        System.out.println(text);
    }
}
