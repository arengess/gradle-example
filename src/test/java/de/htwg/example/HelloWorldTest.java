package de.htwg.example;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by armin on 09/04/15.
 */
public class HelloWorldTest {

    HelloWorld helloWorld;

    @Before
    public void setUp() throws Exception {
        helloWorld = new HelloWorld();
    }

    @Test
    public void testSayHello() throws Exception {
        helloWorld.sayHello("Hello from Test");
    }
}